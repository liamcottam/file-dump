#!/bin/bash

echo "uninstalling FleetSSL"

yum remove letsencrypt-cpanel -y

rpm -e --justdb letsencrypt-cpanel-0.6.1-1

echo "Installing LetsEncrypt"


/scripts/install_lets_encrypt_autossl_provider; whmapi1 set_autossl_provider provider=LetsEncrypt x_terms_of_service_accepted=https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf; whmapi1 set_autossl_metadata_key key=clobber_externally_signed value=1; whmapi1 set_autossl_metadata_key key=notify_autossl_expiry value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_expiry_coverage value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_expiry_coverage_user value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_user value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_coverage value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_coverage_user value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_coverage_reduced value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_coverage_reduced_user value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_uncovered_domains value=0; whmapi1 set_autossl_metadata_key key=notify_autossl_renewal_uncovered_domains_user value=0; whmapi1 update_featurelist featurelist=default autossl=1 ; whmapi1 update_featurelist featurelist=disabled autossl=1; whmapi1 update_featurelist featurelist=disabled backup=1