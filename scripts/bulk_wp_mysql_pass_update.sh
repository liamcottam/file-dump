for user in `cut -d: -f1 /etc/trueuserowners`
    do for domain in `grep -w $user /etc/userdomains | cut -d: -f1`
        do DOCROOT=`whmapi1 domainuserdata domain=$domain --output=json | jq -r '.data | .userdata | .documentroot'`
            echo $DOCROOT
            PASSWORD=`openssl rand -hex 20`
            echo $PASSWORD
            DATABASE=`su -s /bin/bash - $user -c "wp config get DB_USER --path=$DOCROOT"`
            echo $DATABASE
            su -s /bin/bash $user -c "wp config set DB_PASSWORD $PASSWORD --path=$DOCROOT"
            whmapi1 set_mysql_password user=$DATABASE password="$PASSWORD" cpuser=$user
    done
done
