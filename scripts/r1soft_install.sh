#!/bin/sh

# Run script with the backup servers URL, such as
# /scripts/r1soft_install.sh http://neptune.cloudns.io

cat <<EOT >> /etc/yum.repos.d/r1soft.repo
[r1soft]
name=R1Soft Repository Server
baseurl=http://repo.r1soft.com/yum/stable/\$basearch/
enabled=1
gpgcheck=0 
EOT

yum install serverbackup-enterprise-agent -y
yum install kernel-devel-$(uname -r) -y

r1soft-setup --get-key $1
r1soft-setup --get-module