#!/bin/bash
if [ "$#" -eq 0 ] ; then
        echo "Please specify the username to proceed"
elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        echo $1|grep '@'
        echo "Generating Report for $arg"
        lve-read-snapshot --user=$arg --period=1d > /tmp/lve_report.txt
        curl --upload-file /tmp/lve_report.txt http://transfer.brixly.uk:8080/lve_report.txt
fi