#!/usr/bin/python

import subprocess
import os
try:
    import simplejson as json
except ImportError:
    import json


lveinfo_out = subprocess.check_output('/usr/sbin/lveinfo --by-fault=pmemf,ep --period=1h -d --limit=5 --show-columns ID,pmemf,epf -j',shell=True)
json_parsed_lveinfo = json.loads(lveinfo_out)
lveinfo_user = json_parsed_lveinfo['data']
for entry in lveinfo_user:
    theuser = entry['ID']
    memfault = entry['PMemF']
    epfault = entry['EPf']
    if memfault > 20000 or epfault > 5000:
        suspended_list = os.listdir("/var/cpanel/suspended")
        if theuser in suspended_list:
            pass
        else:
            faults = "PMEM: " + str(memfault) + " EP: " + str(epfault)
            subprocess.call('/scripts/suspendacct '+theuser+' "High CloudLinux LVE Faults in 1 Hour - ' + faults + '" 1', shell=True)
            subprocess.call('echo "$(date) - '+theuser+' suspended - ' + faults + '" >> /var/log/lve_suspended', shell=True)
            subprocess.call('/usr/sbin/lve-read-snapshot --user='+theuser+' --period=1h >> /var/log/lve_suspended', shell=True)
