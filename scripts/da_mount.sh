#!/bin/bash

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/da_mount.sh)

clear

csf -a 81.19.215.16
yum install nfs-utils -y
mkdir -p /mnt/da_share
mount -t nfs 81.19.215.16:/home /mnt/da_share

echo "Mount Completed - /mnt/da_share"

wget -O /scripts/backup_reseller.sh https://gitlab.com/brixly/file-dump/raw/master/scripts/backup_reseller.sh && chmod 655 /scripts/backup_reseller.sh
