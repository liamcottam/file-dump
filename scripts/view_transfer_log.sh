# !/bin/bash
if [ "$#" -eq 0 ] ; then
        echo "No parameters passed. Hostname and Transfer Session ID required."
elif [ "$#" -eq 1 ] ; then
        echo "Invalid number of parameters. Hostname and Transfer Session ID required."
elif [ "$#" -eq 2 ] ; then
        hostname=$(echo $1)
        session=$(echo $2)
        ssh -o "StrictHostKeyChecking no" root@$hostname "ls /usr/local/cpanel/bin/view_transfer $session"
        #fi
else
        echo "Invalid number of parameters"
fi
