#!/usr/bin/python3

import json
import subprocess

# Get all resellers
resellers=json.loads(subprocess.getoutput("whmapi1 listresellers --output=json"))
for reseller in resellers['data']['reseller']:
    # I don't like making a second call here but it's the only way to get the package
    accountinfo=json.loads(subprocess.getoutput(f"whmapi1 accountsummary --output=json user={reseller}"))['data']['acct']
    if accountinfo[0]['plan'] == "Premium Reseller":
        # If it is a premium reseller exit the loop
        print(f"{reseller} is a Premium Reseller")
        subprocess.getoutput(f"whmapi1 setacls reseller={reseller} acllist='Premium Reseller'")
        continue
    # This gets current and limit of accounts
    resellerinfo=json.loads(subprocess.getoutput(f"whmapi1 acctcounts user={reseller}  --output=json"))['data']['reseller']
    # For some reason some account limit values are a string and some are an int, if we try and cast to int it fails
    # if the limit is null
    try:
        if int(resellerinfo['limit']) == 20:
            print(f"{reseller} is an Entry level account, it has a limit of {resellerinfo['limit']}, setting it to use Reseller Entry ACL.")
            subprocess.getoutput(f"whmapi1 setacls reseller={reseller} acllist='Reseller Entry'")
        elif int(resellerinfo['limit']) == 50 or int(resellerinfo['limit']) == 100 or int(resellerinfo['limit']) == 150 or int(resellerinfo['limit']) == 250:
            print(f"{reseller} has a Pro account, it has a limit of {resellerinfo['limit']}, setting it to use Reseller Pro ACL.")
            subprocess.getoutput(f"whmapi1 setacls reseller={reseller} acllist='Reseller Pro'")
        else:
            print(f"{reseller} doesn't have a normal package limit, it has a limit of {resellerinfo['limit']}, please check this!")
    except:
        print(f"!WARNING {reseller} doesn't have a limit set WARNING!")
