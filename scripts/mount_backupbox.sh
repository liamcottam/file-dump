#/bin/bash

if [ $# -eq 0 ]; then
    printf "Please enter the Hetzner Storage Box 'Username' as parameter 1, then the password as parameter 2\n\n"
    printf "For example...\n\n"
    printf "/scripts/mount_backupbox.sh u201249 PASSWORDHERE\n\n"
    exit 1
fi

# Check if the sshfs is found, and if not, install it...

if ! command -v sshfs &> /dev/null
then
    printf "Installing sshfs..."
    yum install sshfs fuse-sshfs -y
    exit
fi

printf "Creating mount point /mnt/backup-box"
mkdir -p /mnt/backup-box
echo $2 | sshfs $1@$1.your-storagebox.de:/  /mnt/backup-box -o password_stdin,allow_other,StrictHostKeyChecking=no
