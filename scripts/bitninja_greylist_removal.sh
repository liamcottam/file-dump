# /bin/bash
# Loop through all IP addresses, and remove from greylist

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/bitninja_greylist_removal.sh)

if [ ! -f /scripts/bitninja_greylist_removal.sh ]; then
    printf "Adding Script to /scripts/bitninja_greylist_removal.sh\n"
    wget -O /scripts/bitninja_greylist_removal.sh https://gitlab.com/brixly/file-dump/raw/master/scripts/bitninja_greylist_removal.sh
    chmod 655 /scripts/bitninja_greylist_removal.sh
fi

if [ ! -f /etc/cron.d/bitninja_greylist_removal ]; then
    printf "Adding Cronjob Entry\n"
    echo "*/5 * * * * /bin/bash /scripts/bitninja_greylist_removal.sh > /dev/null 2>&1" > /etc/cron.d/bitninja_greylist_removal
fi

for ip in $(ip addr | grep inet | awk '{print $2}' | awk -F"/" '{print $1}')
    do 
        bitninjacli --greylist --del=$ip;
    done
