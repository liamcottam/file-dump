[mysql]
port                            = 3306
socket                          = /var/lib/mysql/mysql.sock

[mysqld]
# Required Settings
#bind_address                    = 127.0.0.1
datadir                         = /var/lib/mysql/
max_allowed_packet              = 256M
max_connect_errors              = 1000000
pid_file                        = /var/lib/mysql/mysql.pid
port                            = 3306
skip_external_locking
socket                          = /var/lib/mysql/mysql.sock
tmpdir                          = /tmp
user                            = mysql
sql_mode                       = "NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"

# InnoDB Settings
default_storage_engine          = InnoDB
innodb_buffer_pool_instances    = 32     # Use 1 instance per 1GB of InnoDB pool size
innodb_buffer_pool_size         = 64G    # Use up to 70-80% of RAM & optionally check if /proc/sys/vm/swappiness is set to 0
innodb_file_per_table           = 1
innodb_flush_log_at_trx_commit  = 0
innodb_flush_method             = O_DIRECT
innodb_log_buffer_size          = 16M
innodb_log_file_size            = 512M
innodb_stats_on_metadata        = 0

#innodb_temp_data_file_path     = ibtmp1:64M:autoextend:max:20G # Control the maximum size for the ibtmp1 file
#innodb_thread_concurrency      = 4     # Optional: Set to the number of CPUs on your system (minus 1 or 2) to better
                                        # contain CPU usage. E.g. if your system has 8 CPUs, try 6 or 7 and check
                                        # the overall load produced by MySQL/MariaDB.
innodb_read_io_threads          = 64
innodb_write_io_threads         = 64

# MyISAM Settings
query_cache_limit               = 16M    # UPD
query_cache_size                = 128M   # UPD
query_cache_type                = 0

key_buffer_size                 = 256M   # UPD

low_priority_updates            = 1
concurrent_insert               = 2

# Connection Settings
max_connections                 = 600   # UPD

back_log                        = 512
thread_cache_size               = 100
thread_stack                    = 192K

interactive_timeout             = 180
wait_timeout                    = 180

# Buffer Settings
join_buffer_size                = 12M    # UPD
read_buffer_size                = 9M    # UPD
read_rnd_buffer_size            = 12M    # UPD
sort_buffer_size                = 12M    # UPD

table_definition_cache          = 20000 # UPD
table_open_cache                = 20000 # UPD
open_files_limit                = 60000 # UPD

max_heap_table_size             = 128M
tmp_table_size                  = 128M

# Search Settings
ft_min_word_len                 = 3     # Minimum length of words to be indexed for search results

# Logging
log_error                       = /var/lib/mysql/mysql_error.log
log_queries_not_using_indexes   = 0
#long_query_time                 = 5
slow_query_log                  = 0     # Disabled for production
slow_query_log_file             = /var/lib/mysql/mysql_slow.log

[mysqldump]
# Variable reference
# For MySQL 5.7: https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html
# For MariaDB:   https://mariadb.com/kb/en/library/mysqldump/
quick
quote_names
max_allowed_packet              = 64M